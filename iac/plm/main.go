//revive:disable:package-comments
package main

import (
	"context"
	"os"
	"path/filepath"
	"plm/util"

	"dagger.io/dagger"
)

func main() {

	ctx := context.Background()

	c, err := dagger.Connect(ctx, dagger.WithLogOutput(os.Stdout))
	if err != nil {
		panic(err)
	}
	defer c.Close()

	c = c.Pipeline("cd-inline-source")
	id, err := c.
		Container().
		// From("busybox:glibc").
		From("golang:alpine").
		WithMountedTemp("/mountedtmp").
		ID(ctx)
	if err != nil {
		panic(err)
	}

	dir, _ := os.Getwd()
	p := filepath.Join(dir, "..", "repo")
	if err != nil {
		panic(err)
	}

	mountedDir := "/mountedtmp"
	id, err = util.MountedHostDirectory(c, id, p, mountedDir).
		ID(ctx)
	if err != nil {
		panic(err)
	}

	pat := os.Getenv("PULUMI_ACCESS_TOKEN")
	gl := os.Getenv("GITLAB_TOKEN")
	id, err = util.PulumiInstall(c, id).
		Pipeline("pulumi-gl").
		WithWorkdir(mountedDir).
		WithEnvVariable("PULUMI_SKIP_UPDATE_CHECK", "true").
		WithEnvVariable("PULUMI_CONFIG_PASSPHRASE", "").
		WithEnvVariable("PULUMI_ACCESS_TOKEN", pat).
		WithEnvVariable("GITLAB_TOKEN", gl).
		WithExec([]string{"pulumi", "login"}).
		ID(ctx)
	if err != nil {
		panic(err)
	}

	_, err = c.Container(dagger.ContainerOpts{ID: id}).
		Pipeline("pulumi-gl-inline-source1").
		WithWorkdir(mountedDir).
		WithExec([]string{"go", "run", "-v", "repoglinline.go"}).
		Stdout(ctx)
	if err != nil {
		panic(err)
	}
}
