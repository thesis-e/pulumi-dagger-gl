//revive:disable:package-comments,exported
package main

import (
	"context"
	"log"
	"os"

	"github.com/pulumi/pulumi-gitlab/sdk/v4/go/gitlab"
	"github.com/pulumi/pulumi/sdk/v3/go/auto"
	"github.com/pulumi/pulumi/sdk/v3/go/auto/debug"
	"github.com/pulumi/pulumi/sdk/v3/go/auto/optdestroy"
	"github.com/pulumi/pulumi/sdk/v3/go/auto/optpreview"
	"github.com/pulumi/pulumi/sdk/v3/go/auto/optup"
	"github.com/pulumi/pulumi/sdk/v3/go/common/tokens"
	"github.com/pulumi/pulumi/sdk/v3/go/common/workspace"
	"github.com/pulumi/pulumi/sdk/v3/go/pulumi"
)

func main() {

	destroy := false
	args := os.Args[1:]
	if len(args) > 0 {
		if args[0] == "destroy" {
			destroy = true
		}
	}

	ctx := context.Background()

	projectName := "thesis-e"
	stackName := "glProject"
	desc := "A inline source Go Pulumi program that creates a GitLab project"
	ws, err := auto.NewLocalWorkspace(ctx, auto.Project(workspace.Project{
		Name:        tokens.PackageName(projectName),
		Runtime:     workspace.NewProjectRuntimeInfo("go", nil),
		Description: &desc,
	}))
	if err != nil {
		panic(err)
	}

	prj, err := ws.ProjectSettings(ctx)
	if err != nil {
		panic(err)
	}

	stack, err := auto.NewStackInlineSource(ctx, stackName, prj.Name.String(), func(ctx *pulumi.Context) error {
		return nil
	})
	if err != nil && auto.IsCreateStack409Error(err) {
		log.Println("stack " + stackName + " already exists")
		stack, err = auto.UpsertStackInlineSource(ctx, stackName, prj.Name.String(), func(ctx *pulumi.Context) error {
			return nil
		})
		if err != nil {
			panic(err)
		}
	}
	if err != nil && !auto.IsCreateStack409Error(err) {
		panic(err)
	}

	pat := os.Getenv("PULUMI_ACCESS_TOKEN")
	err = stack.Workspace().SetEnvVars(map[string]string{
		"PULUMI_SKIP_UPDATE_CHECK": "true",
		"PULUMI_CONFIG_PASSPHRASE": "",
		"PULUMI_ACCESS_TOKEN":      pat,
	})
	if err != nil {
		panic(err)
	}

	gl := os.Getenv("GITLAB_TOKEN")
	err = stack.SetAllConfig(ctx, auto.ConfigMap{
		"gitlab:token": auto.ConfigValue{
			Value:  gl,
			Secret: true,
		},
	})
	if err != nil {
		panic(err)
	}

	if destroy {

		drst, err := stack.Destroy(ctx, optdestroy.Message("Successfully destroyed stack : "+stackName))
		if err != nil {
			panic(err)
		}
		log.Println(drst.Summary.Kind + " " + drst.Summary.Message)

		return
	}

	stack.Workspace().SetProgram(func(pCtx *pulumi.Context) error {

		groupName := "thesis-e"
		groupID, err := gitlab.LookupGroup(pCtx, &gitlab.LookupGroupArgs{
			FullPath: pulumi.StringRef(groupName),
		}, nil)
		if err != nil {
			return err
		}

		glPrj, err := gitlab.NewProject(pCtx, "newProjectGl", &gitlab.ProjectArgs{
			Description:     pulumi.String("A GitLab project created by Dagger CI/CD & Pulumi"),
			Name:            pulumi.String("pulumi-dagger-gl"),
			NamespaceId:     pulumi.Int(groupID.GroupId),
			VisibilityLevel: pulumi.String("public"),
		})
		if err != nil {
			return err
		}

		pCtx.Export("projectName", glPrj.Name)
		pCtx.Export("projectID", glPrj.WebUrl)

		return nil

	})

	prev, err := stack.Preview(ctx, optpreview.Message("Preview stack "+stackName), optpreview.DebugLogging(debug.LoggingOptions{
		Debug: true,
	}))
	if err != nil {
		panic(err)
	}
	log.Println(prev.StdOut)

	up, err := stack.Up(ctx, optup.Message("Update stack "+stackName), optup.DebugLogging(debug.LoggingOptions{
		Debug: true,
	}))
	if err != nil {
		panic(err)
	}
	log.Println(up.StdOut)

}
