//revive:disable:package-comments
package main

import (
	"ci/lint"
	"ci/util"
	"context"
	"log"
	"os"
	"path/filepath"

	"dagger.io/dagger"
)

func main() {

	ctx := context.Background()

	c, err := dagger.Connect(ctx, dagger.WithLogOutput(os.Stdout))
	if err != nil {
		panic(err)
	}
	defer c.Close()

	c = c.Pipeline("ci")
	id, err := c.
		Container().
		From("busybox:uclibc").
		WithMountedTemp("/mountedtmp").
		ID(ctx)
	if err != nil {
		panic(err)
	}

	dir, _ := os.Getwd()
	p := filepath.Join(dir, "..")
	if err != nil {
		panic(err)
	}

	mountedDir := "/mountedtmp"
	id, err = util.MountedHostDirectory(c, id, p, mountedDir).
		ID(ctx)
	if err != nil {
		panic(err)
	}

	ec, err := lint.Ec(c, id).
		Pipeline("ec").
		WithWorkdir(mountedDir).
		WithExec([]string{"editorconfig-checker", "-verbose"}).
		Stdout(ctx)
	if err != nil {
		panic(err)
	}
	log.Println(ec)

	idY, err := c.
		Pipeline("yamllint-c").
		Container().
		From("pipelinecomponents/yamllint").
		WithMountedTemp("/code").
		ID(ctx)
	if err != nil {
		panic(err)
	}

	mountedDirY := "/code"
	idY, err = util.MountedHostDirectory(c, idY, p, mountedDirY).
		ID(ctx)
	if err != nil {
		panic(err)
	}

	yl, err := util.MountedHostDirectory(c, idY, p, mountedDir).
		Pipeline("yamllint-run").
		WithWorkdir(mountedDir).
		WithExec([]string{"yamllint", "--no-warnings", "."}).
		Stdout(ctx)
	if err != nil {
		panic(err)
	}
	log.Println(yl)

	revive, err := lint.Revive(c, id).
		Pipeline("revive").
		WithWorkdir(mountedDir).
		WithExec([]string{"revive", "-set_exit_status", "./..."}).
		Stdout(ctx)
	if err != nil {
		panic(err)
	}
	log.Println(revive)
}
